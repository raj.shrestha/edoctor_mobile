(function(){
  angular.module('starter')
  .controller('CallController', ['localStorageService', '$scope', '$timeout', '$ionicPopup', 'EXT_URL', 'IceServerService', CallController]);

  function CallController(localStorageService, $scope, $timeout, $ionicPopup, EXT_URL, IceServerService){
    $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
      viewData.enableBack = true;
    });
    $scope.showCallBtn = true;
    $scope.showEndCallBtn = false;
    $scope.username = localStorageService.get('uid');
    $scope.userId = localStorageService.get('uid');
    $scope.contact_username = localStorageService.get('contactName');
    $scope.contactId = localStorageService.get('contactId');
    $scope.call = null;
    $scope.mediaStream = null;
    var localVideo = document.querySelector('#local-video');
    var customConfig;
    IceServerService.fetchConfig().then(function(data) {
      customConfig = data;
    }, function(err) {
      console.log('error to fetch ice config');
    });
    customConfig = customConfig || {'iceServers': [
                { url: 'stun:stun1.l.google.com:19302' },
                { url: 'turn:numb.viagenie.ca', credential: 'muazkh', username: 'webrtc@live.com' }
            ]};
		// get the local video and audio stream and show preview in the
	  // "LOCAL" video element
	  // successCb: has the signature successCb(stream); receives
	  // the local video stream as an argument
	  try {
      // create connection to the ID server
       $scope.peer = new Peer($scope.userId, {
       /*production*/
       key: EXT_URL.peer_server_key,
       config: customConfig,
       /*local*/
       // host: '192.168.0.102',
       // port: '9000',
       // path: '/peerjs',
       // // Set highest debug level (log everything!).
       // debug: 3,
       // // Set a logging function:
       // logFunction: function() {
       //   var copy = Array.prototype.slice.call(arguments).join(' ');
       //   // $('.log').append(copy + '<br>');
       //   console.log(copy);
       // }
       //localhost IP
      });

      // hack to get around the fact that if a server connection cannot
      // be established, the peer and its socket property both still have
      // open === true; instead, listen to the wrapped WebSocket
      // and show an error if its readyState becomes CLOSED
      $scope.peer.socket._socket.onclose = function () {
        console.log('no connection to server');
        $scope.peer = null;
      };

      // get local stream ready for incoming calls once the wrapped
      // WebSocket is open
      $scope.peer.socket._socket.onopen = function () {
        // getLocalStream();
      };

      // handle events representing incoming calls
      // $scope.peer.on('call', answer);
    }
     catch (e) {
      $scope.peer = null;
      console.log('error while connecting to server');
      console.log(e.message);
    }

    function getVideo(successCallback, errorCallback){
    	navigator.getUserMedia = navigator.getUserMedia ||
		                         	 navigator.webkitGetUserMedia ||
		                         	 navigator.mozGetUserMedia;
      if (navigator.getUserMedia) {
      	navigator.getUserMedia({audio: true, video: true}, successCallback, errorCallback);
			} else {
			   console.log("getUserMedia not supported");
			}
    }

    function setConnectionCloseAction(){
      $scope.call.on('close', function(){
        $scope.mediaStream.stop();
        $scope.showCallBtn = true;
        $scope.showEndCallBtn = false;
        $ionicPopup.alert({
          title: 'Call Ended',
          template: 'Your call is ended'
        });
      })
    }

    //reciver side
    function onReceiveCall(call){
    	$scope.call = call;
      setConnectionCloseAction();
      var clickedConform = false;
      var aceptCall = function() {
        getVideo(
          function(MediaStream){
            	$scope.mediaStream = MediaStream;
              $scope.call.answer($scope.mediaStream);
            },
            function(err){
                $ionicPopup.alert({
                    title: 'Error',
                    template: 'An error occurred while try to connect to the device mic and camera'
                });
            }
        );
        $scope.call.on('stream', onReceiveStream);
      }

      // A confirm dialog
      var confirmPopup = $ionicPopup.confirm({
       title: 'Incoming Call',
       template: 'Someone is calling you. Connecting now..'
      });

      confirmPopup.then(function(res) {
       clickedConform = true;
       if(res) {
         callRecieved = true;
         aceptCall();
       } else {
         console.log('cancel call');
       }
      });
      $timeout(function(){
        if(!clickedConform){
          confirmPopup.close();
          $ionicPopup.alert({
            title: 'call end',
            template: 'no response'
          });
        }
      }, 9000);
    }


    function onReceiveStream(stream){
      var video = document.getElementById('contact-video');
      video.src = window.URL.createObjectURL(stream);
      $scope.showCallBtn = false;
      $scope.showEndCallBtn = true;
      video.onloadedmetadata = function(){
        $scope.callRecieved = true;
        if($scope.callingPopup)
          $scope.callingPopup.close();
        $ionicPopup.alert({
          title: 'Call Ongoing',
          template: 'Call has started. You can speak now'
        });
      };

    }

    //starting call
    $scope.startCall = function(){
        // var contact_username = $scope.contact_username;
      $scope.callingPopup = $ionicPopup.show({
        title: 'Calling',
        template: 'Calling to contact user'
      });
      $scope.callRecieved = false;
      getVideo(
          function(MediaStream){
          	$scope.mediaStream = MediaStream;
            $scope.call = $scope.peer.call($scope.contactId, $scope.mediaStream);
            setConnectionCloseAction();
            $scope.call.on('stream', onReceiveStream);
          },
          function(err){
              $ionicPopup.alert({
                  title: 'Error',
                  template: 'An error occurred while try to connect to the device mic and camera'
              });
          }
      );
      $timeout(function(){
        if(!$scope.callRecieved) {
          $scope.callingPopup.close();
          $scope.call.close();
          $scope.mediaStream.stop();
          $ionicPopup.alert({
            title: 'not respond',
            template: 'no response from contact user'
          });
        }
      }, 20000);
    };

    $scope.endCall = function(){
    	var confirmPopup = $ionicPopup.confirm({
                title: 'End the call',
                template: 'Do you wnat to end the call?'
            });
      confirmPopup.then(function(res) {
       if(res) {
        $scope.call.close();
       } else {
         console.log('cancel call');
       }
      })
    };

    $scope.peer.on('call', onReceiveCall);

  }

})();
