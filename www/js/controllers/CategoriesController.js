(function(){
  angular.module('starter')
  .controller('CategoriesController',
  function ($stateParams, $scope, $state, $ionicPopup, UsersService){
    $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
      viewData.enableBack = true;
    });
    $scope.categoryName = $stateParams.catName;
    // $scope.login = function(){

    //   var username = $scope.username;
    //   localStorageService.set('username', username);
    //   $state.go('app');

    // };
    UsersService.specializations().then(function(data){
      $scope.specializations = data;
    }, function(err) {
      var alertPopup = $ionicPopup.alert({
        title: 'failed!',
        template: err
      });
    });

  });

})();
