(function() {
  angular.module('starter')
  .config(function ($stateProvider, $urlRouterProvider, USER_ROLES) {
    $stateProvider
    .state('welcome', {
      url: '/welcome',
      templateUrl: 'templates/welcome.html',
      controller: 'WelcomeCtrl'
    })
    .state('signup',{
      url: '/signup',
      templateUrl: 'templates/authentication/signup.html',
      controller: 'SignupCtrl',
      data: {
        authorizedRoles: [USER_ROLES.public]
      }
    })
    .state('login', {
      url: '/login',
      templateUrl: 'templates/authentication/login.html',
      controller: 'LoginCtrl',
      data: {
        authorizedRoles: [USER_ROLES.public]
      }
    })
    .state('main', {
      url: '/',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'MenuCtrl'
    })
    .state('main.tabs', {
      url: "tabs",
      // templateUrl: "templates/users_tab.html"
      views: {
        'menuContent': {
          templateUrl: 'templates/users_tab.html'
        }
      }
    })
    .state('main.dash', {
      url: '/dash',
      views: {
          'menuContent': {
            templateUrl: 'templates/dashboard.html',
            // controller: 'UsersCtrl'
          }
      }
    })
    .state('main.tabs.users', {
      url: '/users',
      cache: false,
      views: {
        'home-tab': {
          templateUrl: 'templates/users.html',
          controller: 'UsersCtrl'
        }
      }
    })
    .state('main.user', {
      url: 'users/:userID',
      views: {
        'menuContent': {
          templateUrl: 'templates/profile/doctor.html'
        }
      }
    })
    .state('main.tabs.categories', {
      url: '/categories',
      cache: false,
      views: {
        'category-tab': {
          templateUrl: 'templates/categories/index.html',
          controller: 'CategoriesController'
        }
      }
    })
    .state('main.cat', {
      url: 'categories/:catName',
      cache: false,
      views: {
        'menuContent': {
          templateUrl: 'templates/categories/show.html',
          controller: 'CategoriesController'
        }
      }
    })
    .state('main.tabs.favorite', {
      url: 'favorite',
      cache: false,
      views: {
        'favorite-tab': {
          templateUrl: 'templates/users.html',
          controller: 'UsersCtrl'
        }
      }
    })
    .state('main.public', {
      url: '/public',
      views: {
          'menuContent': {
            templateUrl: 'templates/public.html'
          }
      }
    })
    .state('main.admin', {
      url: '/admin',
      views: {
          'menuContent': {
            templateUrl: 'templates/admin.html'
          }
      },
      data: {
        authorizedRoles: [USER_ROLES.admin]
      }
    })
    .state('main.chat', {
      url: '/chat',
      views: {
          'menuContent': {
            templateUrl: 'templates/videocall.html',
            controller: 'CallController'
          }
      },
      data: {}
    });
    $urlRouterProvider.otherwise('/welcome');
  })
})();
