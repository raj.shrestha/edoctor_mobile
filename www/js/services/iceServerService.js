(function() {
  angular.module('starter')

  .service('IceServerService', function($q, $http) {
    var url = "https://service.xirsys.com/ice";
    var config_data = {
      ident: "rzkmr",
      secret: "300f0d58-8044-11e5-8bfb-732d6b45f546",
      domain: "www.kurakani.com",
      application: "default",
      room: "default",
      secure: 1
    };
    var token = $http.defaults.headers.common['X-Auth-Token'];
    $http.defaults.headers.common={Accept: "application/json, text/plain, */*"}
    
    var fetchConfig = function() {
      return $q(function(resolve, reject) {
        $http.get(url, config_data)
        .success(function (data, status) {
          resolve(data.d);
          // data = JSON.parse(data);
          // if(data.status == 'ok') {
          //   resolve(data;
          // }else {
          //   reject('Failed!.');
          // }
          $http.defaults.headers.common['X-Auth-Token'] = token;
        })
        .error(function(response, status){
          reject('Failed to fetch.');
        });
      });
    };

    return {
      fetchConfig: fetchConfig
    };
  });
})();
