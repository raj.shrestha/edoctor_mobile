(function() {
  angular.module('starter')

  .service('UsersService', function($q, $http, localStorageService, USER_ROLES, EXT_URL, AuthService) {
    var BASE_URL = EXT_URL.base + '/v1';
    var users = function() {
      AuthService.setHeaderToken(null);
      return $q(function(resolve, reject) {
        $http.get(BASE_URL+'/users')
        .success(function (data, status) {
          data = JSON.parse(data);
          if(data.status == 'ok') {
            resolve(data.users);
          }else {
            reject('Failed!.');
          }
        })
        .error(function(response, status){
          reject('Failed to fetch.');
        });
      });
    };

    var specializations = function() {
      AuthService.setHeaderToken(null);
      return $q(function(resolve, reject) {
        $http.get(BASE_URL+'/specializations')
        .success(function (data, status) {
          data = JSON.parse(data);
          if(data.status == 'ok') {
            resolve(data.specializations);
          }else {
            reject('Failed!.');
          }
        })
        .error(function(response, status){
          reject('Failed to fetch.');
        });
      });
    };

    return {
      users: users,
      specializations: specializations
    };
  });
  
})();
