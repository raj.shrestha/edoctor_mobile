(function() {
  angular.module('starter')

  .service('AuthService', function($q, $http, localStorageService, USER_ROLES, EXT_URL) {
    var BASE_URL = EXT_URL.base;
    var LOCAL_TOKEN_KEY = 'aTKey';
    var username = '';
    var isAuthenticated = false;
    var role = '';
    var authToken;

    function loadUserCredentials() {
      var token = localStorageService.get(LOCAL_TOKEN_KEY);
      if (token) {
        useCredentials(token);
      }
    }

    function storeUserCredentials(token, uid, role) {
     localStorageService.set('uid', uid);
     localStorageService.set('role', role);
     localStorageService.set(LOCAL_TOKEN_KEY, token);
      useCredentials(token);
    }

    function useCredentials(token) {
      username = token.split('_')[0];
      localStorageService.set('username', username);
      isAuthenticated = true;
      // authToken = token.split('_')[1];

      if (username == 'admin') {
        role = USER_ROLES.admin
      }
      if (username == 'user') {
        role = USER_ROLES.public
      }

      // Set the token as header for your requests!
      setHeaderToken(token);
    }

    function destroyUserCredentials() {
      authToken = undefined;
      username = '';
      isAuthenticated = false;
      $http.defaults.headers.common['X-Auth-Token'] = undefined;
      localStorageService.remove(LOCAL_TOKEN_KEY);
      localStorageService.remove('username');
      localStorageService.remove('uid');
      localStorageService.remove('role');
      localStorageService.remove('contactName');
      localStorageService.remove('contactId');
    }

    var setHeaderToken = function(token) {
      token = token || localStorageService.get(LOCAL_TOKEN_KEY);
      authToken = token.split('_')[1];
      $http.defaults.headers.common['X-Auth-Token'] = authToken;
    }
    var signup = function(data) {
      profile_type = data.checked ? "Doctor" : "Patient";
      return $q(function(resolve, reject) {
        $http.post(BASE_URL+'/registrations', { 
          firstname: data.firstname, 
          lastname: data.lastname,
          email: data.email,
          profile_type: profile_type,
          license: data.license,
          password: data.password,
          password_confirmation: data.password_confirmation
        })
        .success(function (resData, status) {
          resData = JSON.parse(resData);
          if(resData.status == 'ok') {
            storeUserCredentials(data.email + '_' + resData.token, resData.uid, resData.profile_type);
            resolve('Signup success.');
          }else {
            reject('Signup Failed.');
          }
        })
        .error(function(response, status){
          reject('Error in connection.');
        });
      });
    };

    var login = function(name, pw) {
      return $q(function(resolve, reject) {
        $http.post(BASE_URL+'/sessions', { email: name, password: pw })
        .success(function (data, status) {
          data = JSON.parse(data);
          if(data.status == 'ok') {
            storeUserCredentials(name + '_' + data.token, data.uid, data.profile_type);
            resolve('Login success.');
          }else {
            reject('Login Failed.');
          }
        })
        .error(function(response, status){
          reject('Login Failed.');
        });  
        // if ((name == 'admin' && pw == '1') || (name == 'user' && pw == '1')) {
        //   // Make a request and receive your auth token from your server
        //   storeUserCredentials(name + '.yourServerToken');
        //   resolve('Login success.');
        // } else {
        //   reject('Login Failed.');
        // }
      });
    };

    var logout = function() {
      return $q(function(resolve, reject) {
        var token = localStorageService.get(LOCAL_TOKEN_KEY);
        token = token.split('_')[1];
        $http.get(BASE_URL + "/sessions/" + token)
        .success(function (data, status) {
          data = JSON.parse(data);
          if(data.status == 'ok') {
            destroyUserCredentials();
          }else {
            reject('Failed.');
          }
        })
        .error(function(response){
          reject('Failed.');
        });
      });
    };

    var isAuthorized = function(authorizedRoles) {
      if (!angular.isArray(authorizedRoles)) {
        authorizedRoles = [authorizedRoles];
      }
      if(authorizedRoles[0] === 'public_role') { isAuthenticated = true; }
      return ((isAuthenticated && authorizedRoles.indexOf(role) !== -1) || authorizedRoles[0] === 'public_role');
    };

    loadUserCredentials();

    return {
      signup: signup,
      login: login,
      logout: logout,
      isAuthorized: isAuthorized,
      setHeaderToken: setHeaderToken,
      isAuthenticated: function() {return isAuthenticated;},
      username: function() {return username;},
      role: function() {return role;}
    };
  })

  .factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
    return {
      responseError: function (response) {
        $rootScope.$broadcast({
          401: AUTH_EVENTS.notAuthenticated,
          403: AUTH_EVENTS.notAuthorized
        }[response.status], response);
        return $q.reject(response);
      }
    };
  })

  .config(function ($httpProvider) {
    $httpProvider.interceptors.push('AuthInterceptor');
  });

})();
