angular.module('starter')

// .controller('AppCtrl', function() {})
// .controller('LoginCtrl', function() {})
// .controller('DashCtrl', function() {});
.controller('AppCtrl', function($scope, $state, $ionicPopup, AuthService, AUTH_EVENTS) {
  $scope.username = AuthService.username();

  $scope.$on(AUTH_EVENTS.notAuthorized, function(event) {
    var alertPopup = $ionicPopup.alert({
      title: 'Unauthorized!',
      template: 'You are not allowed to access this resource.'
    });
  });

  $scope.$on(AUTH_EVENTS.notAuthenticated, function(event) {
    AuthService.logout();
    $state.go('welcome');
    var alertPopup = $ionicPopup.alert({
      title: 'Session Lost!',
      template: 'Sorry, You have to login again.'
    });
  });

  $scope.setCurrentUsername = function(name) {
    $scope.username = name;
  };
})
.controller('WelcomeCtrl', function($scope, $state){
  $scope.login = function() {
    $state.go('login');
  };

  $scope.signup = function() {
    $state.go('signup');
  };
})

.controller('SignupCtrl', function($scope, $state, $ionicPopup, localStorageService, AuthService) {
  $scope.data = {};

  $scope.signup = function(data) {
    AuthService.signup(data).then(function(authenticated) {
      profile_type = localStorageService.get('role');
      if(data.profile_type === 'Doctor') {
        $state.go('main.dash', {}, {reload: true});
      }
      else {
        $state.go('main.tabs.users', {}, {reload: true});
      }
      // $scope.setCurrentUsername(data.username);
      $scope.username = localStorageService.get('username');
    }, function(err) {
      var alertPopup = $ionicPopup.alert({
        title: 'Signup failed!',
        template: 'Please check your fields!'
      });
    });
  };
})

.controller('LoginCtrl', function($scope, $state, $ionicPopup, localStorageService, AuthService) {
  $scope.data = {};

  $scope.signup = function() {
    $state.go('signup');
  };

  $scope.login = function(data) {
    AuthService.login(data.username, data.password).then(function(authenticated) {
      profile_type = localStorageService.get('role');
      if(profile_type === 'Doctor') {
        $state.go('main.dash', {}, {reload: true});
      }
      else {
        $state.go('main.tabs.users', {}, {reload: true});
      }
      // $scope.setCurrentUsername(data.username);
      $scope.username = localStorageService.get('username');
    }, function(err) {
      var alertPopup = $ionicPopup.alert({
        title: 'Login failed!',
        template: 'Please check your credentials!'
      });
    });
  };
})
.controller('MenuCtrl', function($scope, $state, $http, $ionicPopover, localStorageService, AuthService) {
  $scope.logout = function() {
    AuthService.logout();
    $state.go('welcome');
  };
  $scope.username = localStorageService.get('username');
  // .fromTemplate() method
  var template = '<ion-popover-view>' +
      '   <ion-header-bar>' +
      '       <h1 class="title">My Popover Title</h1>' +
      '   </ion-header-bar>' +
      '   <ion-content class="padding">' +
      '       My Popover Contents' +
      '   </ion-content>' +
      '</ion-popover-view>';

  $scope.popover = $ionicPopover.fromTemplate(template, {
    scope: $scope
  });
  $scope.closePopover = function() {
    $scope.popover.hide();
  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popover.remove();
  });
  var colors = {
    1: "dark-bg",
    2: "positive-bg",
    3: "calm-bg",
    4: "balanced-bg",
    5: "energized-bg",
    6: "assertive-bg",
    7: "royal-bg"
  }
  function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
    // return Math.floor(Math.random() * max) + min;
  }
  $scope.getBackground = function() {
    num = getRandomInt(1, 8);
    return colors[num];
  }
})

.controller('UsersCtrl', function($scope, $state, $ionicPopup, $timeout, ionicMaterialMotion, ionicMaterialInk, localStorageService, UsersService){
  UsersService.users().then(function(data){
    $scope.users = data;
  }, function(err) {
    var alertPopup = $ionicPopup.alert({
      title: 'failed!',
      template: err
    });
  });

  $scope.chat = function(username, userId) {
    localStorageService.set('contactName', username);
    localStorageService.set('contactId', userId);
    $state.go('main.chat')
  };

  $timeout(function() {
      ionicMaterialMotion.fadeSlideIn({
          selector: '.animate-fade-slide-in .item'
      });
  }, 200);

  // Activate ink for controller
  ionicMaterialInk.displayEffect();
})
.controller('DashCtrl', function($scope, $state, $http, $ionicPopup, AuthService) {
  $scope.logout = function() {
    AuthService.logout();
    $state.go('welcome');
  };

  $scope.performValidRequest = function() {
    $http.get('http://localhost:8100/valid').then(
      function(result) {
        $scope.response = result;
      });
  };

  $scope.performUnauthorizedRequest = function() {
    $http.get('http://localhost:8100/notauthorized').then(
      function(result) {
        // No result here..
      }, function(err) {
        $scope.response = err;
      });
  };

  $scope.performInvalidRequest = function() {
    $http.get('http://localhost:8100/notauthenticated').then(
      function(result) {
        // No result here..
      }, function(err) {
        $scope.response = err;
      });
  };
});
