angular.module('starter')

.constant('EXT_URL', {
  base: 'http://drconn.herokuapp.com/api',
  // base: 'http://127.0.0.1:3000/api',
  peer_server_key: 'ncc191vcqgaxlxr',
})

.constant('AUTH_EVENTS', {
  notAuthenticated: 'auth-not-authenticated',
  notAuthorized: 'auth-not-authorized'
})

.constant('USER_ROLES', {
  admin: 'admin_role',
  protected: 'protected_role',
  public: 'public_role'
});
