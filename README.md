# eDoctor mobile

This is mobile app for eDoctor.
Used Technology and library

  - Ionic framework
  - crosswalk web view library
  - peerjs library

# Start
 - Clone
 - npm install
 - bower install
 - ionic browser add crosswalk@12.41.296.5

 # Run app
 
 ionic serve
